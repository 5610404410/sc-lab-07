package Control;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JCheckBox;

import Model.DataSeatPrice;
import View.TheaterSystemGUI;

public class TheaterManagement {	
	private double[] seatsPrice; 
	private ArrayList<JCheckBox> seats;
	private TheaterSystemGUI gui;
	private double amount;
	private String dateState;
	private String timeState;
	
	private ArrayList<Integer> w1;
	private ArrayList<Integer> w2;
	private ArrayList<Integer> h1;
	private ArrayList<Integer> h2;
	private ArrayList<Integer> h3;
	
	public void setGUI(TheaterSystemGUI gui) {
		this.gui = gui;
	}
	
	public TheaterManagement(){
		seatsPrice = DataSeatPrice.ticketPrices;
		dateState = "Weekday";
		timeState = "10.30-12.30";
		
		w1 = new ArrayList<Integer>();
		w2 = new ArrayList<Integer>();
		h1 = new ArrayList<Integer>();
		h2 = new ArrayList<Integer>();
		h3 = new ArrayList<Integer>();
		
	}
	
	public void checkPrice() {
		if(getDateState().equals("Weekday")){
			if(getTimeState().equals("10.30-12.30")){	this.updatePrice(w1); }
			else if(getTimeState().equals("12.30-14.30")){ this.updatePrice(w2); }
		}
		else {
			if(getTimeState().equals("10.30-12.30")){ this.updatePrice(h1); }
			else if(getTimeState().equals("12.30-14.30")){ this.updatePrice(h2); }
			else if(getTimeState().equals("17.00-19.00")){ this.updatePrice(h3); }
		}
	}
	private void updatePrice(ArrayList<Integer> s){
		for (int i = 0; i < 300; i++){
			if (seats.get(i).isSelected()){
				amount += seatsPrice[i];
				seatsPrice[i] = 0;
				seats.get(i).setEnabled(false);
				
				if(!s.contains(i)){
					s.add(i);
				}
			}
		}
		updateSeat(s);
	}
	public void clearPrice() {
		amount = 0;
	}	
	public void getSeats() {
		seats = gui.setSeats();
	}
	public double getTotal() {
		return amount;
	}
	public String getTimeState(){
		return timeState;
	}
	public String getDateState(){
		return dateState;
	}
	public void updateState(String newDState, String newTState){
		dateState = newDState;
		timeState = newTState;
		System.out.println("Change state " + dateState + " " + timeState);
		if(dateState.equals("Weekday")){
			if(timeState.equals("10.30-12.30")){	updateSeat(w1); }
			else if(timeState.equals("12.30-14.30")){ 
				System.out.println("pre-update seat");
				updateSeat(w2); }
		}
		else {
			if(timeState.equals("10.30-12.30")){ updateSeat(h1); }
			else if(timeState.equals("12.30-14.30")){ updateSeat(h2); }
			else if(timeState.equals("17.00-19.00")){ updateSeat(h3); }
		}
	}
	private void updateSeat(ArrayList<Integer> a){
		System.out.println("update seat");
		for(JCheckBox c : seats){
			c.setSelected(false);
			c.setEnabled(true);
		}
		for(Integer i : a){
			seats.get(i).setSelected(true);
			seats.get(i).setEnabled(false);
		}
		
	}
}
