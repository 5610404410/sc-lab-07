package Main;

import Control.TheaterManagement;
import View.TheaterSystemGUI;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub 
		TheaterManagement control = new TheaterManagement();
		TheaterSystemGUI gui = new TheaterSystemGUI(control);
		control.setGUI(gui);
	}

}
