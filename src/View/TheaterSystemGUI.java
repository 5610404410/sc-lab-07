package View;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Control.TheaterManagement;

public class TheaterSystemGUI{

	private ArrayList<JCheckBox> seats;
	private TheaterManagement control;
	private JLabel total;
	private JComboBox<String> day;
	private JComboBox<String> timew;
	private JComboBox<String> timeh;
	private int key;
	private JFrame j;
	
	public TheaterSystemGUI(TheaterManagement control) {
		// TODO Auto-generated constructor stub
		key=1;
		this.control = control;		
		createFrame();
	}
	public void createFrame(){
		// TODO Auto-generated method stub
		seats = new ArrayList<JCheckBox>();
		j = new JFrame();
		j.setDefaultCloseOperation(j.EXIT_ON_CLOSE);
		j.setSize(1300, 700);
		j.setLayout(null);
		
		JPanel p1 = new JPanel();
		
		p1.setLayout(null);
		
		p1.setBounds(1, 1, 1282, 550);
		
		p1.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel l0 = new JLabel("A");
		JLabel l1 = new JLabel("B");
		JLabel l2 = new JLabel("C");
		JLabel l3 = new JLabel("D");
		JLabel l4 = new JLabel("E");
		JLabel l5 = new JLabel("F");
		JLabel l6 = new JLabel("G");
		JLabel l7 = new JLabel("H");
		JLabel l8 = new JLabel("I");
		JLabel l9 = new JLabel("L");
		JLabel l10 = new JLabel("M");
		JLabel l11 = new JLabel("N");
		JLabel l12 = new JLabel("O");
		JLabel l13 = new JLabel("P");
		JLabel l14 = new JLabel("Q");
		
		l0.setBounds(15, 45, 20, 20);
		l1.setBounds(15, 75, 20, 20);
		l2.setBounds(15, 105, 20, 20);
		l3.setBounds(15, 135, 20, 20);
		l4.setBounds(15, 165, 20, 20);
		l5.setBounds(15, 195, 20, 20);
		l6.setBounds(15, 225, 20, 20);
		l7.setBounds(15, 255, 20, 20);
		l8.setBounds(15, 285, 20, 20);
		l9.setBounds(15, 315, 20, 20);
		l10.setBounds(15, 345, 20, 20);
		l11.setBounds(15, 375, 20, 20);
		l12.setBounds(15, 405, 20, 20);
		l13.setBounds(15, 435, 20, 20);
		l14.setBounds(15, 465, 20, 20);
		
		
		int y = 15;
		for(int i = 0; i <= 20; i++) {
			JLabel x = new JLabel(String.valueOf(i));
			x.setBounds(y, 5, 40, 40);
			y += 60;
			p1.add(x);			
		}
		int x = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("A"+String.valueOf(i));
			b.setBounds(x, 45, 60, 20);
			b.setBackground(new Color(255,62,150));
			x += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("A1") || b.getText().equals("A2") || b.getText().equals("A9") || b.getText().equals("A10") || b.getText().equals("A19") || b.getText().equals("A20")){			
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int z = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("B"+String.valueOf(i));
			b.setBounds(z, 75, 60, 20);
			b.setBackground(new Color(255,62,150));
			z += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("B1") || b.getText().equals("B9") || b.getText().equals("B10") || b.getText().equals("B20")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int r = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("C"+String.valueOf(i));
			b.setBounds(r, 105, 60, 20);
			b.setBackground(new Color(255,62,150));
			r += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("C9") || b.getText().equals("C10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int p = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("D"+String.valueOf(i));
			b.setBounds(p, 135, 60, 20);
			b.setBackground(new Color(255,211,155));
			p += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("D1") || b.getText().equals("D2") || b.getText().equals("D19") || b.getText().equals("D20")){
				b.setBackground(new Color(255,62,150));
			}
			if (b.getText().equals("D9") || b.getText().equals("D10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int q = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("E"+String.valueOf(i));
			b.setBounds(q, 165, 60, 20);
			b.setBackground(new Color(255,211,155));
			q += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("E1") || b.getText().equals("E2") || b.getText().equals("E19") || b.getText().equals("E20")){
				b.setBackground(new Color(255,62,150));
			}
			if (b.getText().equals("E9") || b.getText().equals("E10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int s = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("F"+String.valueOf(i));
			b.setBounds(s, 195, 60, 20);
			b.setBackground(new Color(255,211,155));
			s += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("F1") || b.getText().equals("F2") || b.getText().equals("F19") || b.getText().equals("F20")){
				b.setBackground(new Color(255,62,150));
			}
			if (b.getText().equals("F9") || b.getText().equals("F10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int t = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("G"+String.valueOf(i));
			b.setBounds(t, 225, 60, 20);
			b.setBackground(new Color(171,130,255));
			t += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("G1") || b.getText().equals("G2") || b.getText().equals("G3") || b.getText().equals("G18") || b.getText().equals("G19") || b.getText().equals("G20")){
				b.setBackground(new Color(255,62,150));
			}
			if (b.getText().equals("G9") || b.getText().equals("G10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int w = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("H"+String.valueOf(i));
			b.setBounds(w, 255, 60, 20);
			b.setBackground(new Color(84,255,159));
			w += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("H1") || b.getText().equals("H2") || b.getText().equals("H3") || b.getText().equals("H18") || b.getText().equals("H19") || b.getText().equals("H20")){
				b.setBackground(new Color(171,130,255));
			}
			if (b.getText().equals("H9") || b.getText().equals("H10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int d = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("I"+String.valueOf(i));
			b.setBounds(d, 285, 60, 20);
			b.setBackground(new Color(84,255,159));
			d += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("I1") || b.getText().equals("I2") || b.getText().equals("I3") || b.getText().equals("I18") || b.getText().equals("I19") || b.getText().equals("I20")){
				b.setBackground(new Color(171,130,255));
			}
			if (b.getText().equals("I9") || b.getText().equals("I10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int v = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("L"+String.valueOf(i));
			b.setBounds(v, 315, 60, 20);
			b.setBackground(new Color(84,255,159));
			v += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("L1") || b.getText().equals("L2") || b.getText().equals("L3") || b.getText().equals("L18") || b.getText().equals("L19") || b.getText().equals("L20")){
				b.setBackground(new Color(171,130,255));
			}
			if (b.getText().equals("L9") || b.getText().equals("L10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int m = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("M"+String.valueOf(i));
			b.setBounds(m, 345, 60, 20);
			b.setBackground(new Color(84,255,159));
			m += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("M1") || b.getText().equals("M2") || b.getText().equals("M3") || b.getText().equals("M18") || b.getText().equals("M19") || b.getText().equals("M20")){
				b.setBackground(new Color(171,130,255));
			}
			if (b.getText().equals("M9") || b.getText().equals("M10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int n = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("N"+String.valueOf(i));
			b.setBounds(n, 375, 60, 20);
			b.setBackground(new Color(84,255,159));
			n += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("N1") || b.getText().equals("N2") || b.getText().equals("N3") || b.getText().equals("N18") || b.getText().equals("N19") || b.getText().equals("N20")){
				b.setBackground(new Color(171,130,255));
			}
			if (b.getText().equals("N9") || b.getText().equals("N10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int u = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("O"+String.valueOf(i));
			b.setBounds(u, 405, 60, 20);
			b.setBackground(new Color(255,193,37));
			u += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("O1") || b.getText().equals("O2") || b.getText().equals("O3") || b.getText().equals("O18") || b.getText().equals("O19") || b.getText().equals("O20")){
				b.setBackground(new Color(84,255,159));
			}
			if (b.getText().equals("O9") || b.getText().equals("O10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int e = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("P"+String.valueOf(i));
			b.setBounds(e, 435, 60, 20);
			b.setBackground(new Color(255,193,37));
			e += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("P1") || b.getText().equals("P2") || b.getText().equals("P3") || b.getText().equals("P18") || b.getText().equals("P19") || b.getText().equals("P20")){
				b.setBackground(new Color(84,255,159));
			}
			if (b.getText().equals("P9") || b.getText().equals("P10")){
				b.setVisible(false);
				b.setEnabled(false);
			}
		}
		int f = 53;
		for (int i = 1; i <= 20; i++) {
			JCheckBox b = new JCheckBox("Q"+String.valueOf(i));
			b.setBounds(f, 465, 60, 20);
			b.setBackground(new Color(255,193,37));
			f += 60;
			p1.add(b);
			seats.add(b);
			if (b.getText().equals("Q1") || b.getText().equals("Q2") || b.getText().equals("Q3") || b.getText().equals("Q18") || b.getText().equals("Q19") || b.getText().equals("Q20")){
				b.setBackground(new Color(84,255,159));
			}
		}
		
		JButton buy = new JButton("Buy");
		JButton clear = new JButton("Clear");
		
		buy.setBounds(630, 590, 80, 30);
		clear.setBounds(980, 590, 80, 30);
		
		buy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				control.getSeats();
				control.checkPrice();
				showTotal();
			}
		});
		
		clear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				control.clearPrice();
				showTotal();
			}
		});
		
		total = new JLabel("Total Price :");
		
		total.setBounds(730, 555, 180, 100);
		
		day = new JComboBox<String>();
		day.addItem("WeekDay");
		day.addItem("Holiday");
		
		day.setBounds(20, 590, 180, 30);
		
		timew = new JComboBox<String>();
		timew.addItem("10.30-12.30");
		timew.addItem("12.30-14.30");
		
		timew.setBounds(300, 590, 200, 30);
		
		timeh = new JComboBox<String>();
		timeh.addItem("10.30-12.30");
		timeh.addItem("12.30-14.30");
		timeh.addItem("17.00-19.00");
		
		timeh.setBounds(300, 590, 200, 30);
		
		JButton ok1 = new JButton("OK");
		ok1.setBounds(220, 590, 55, 30);
		
		ok1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(control.getDateState().equals("Holiday") && 
						day.getSelectedItem().toString().equals("WeekDay")){
					
						control.updateState("Weekday", "10.30-12.30");
						j.remove(timeh);
						j.add(timew);
				}
				else if(control.getDateState().equals("Weekday") && 
						day.getSelectedItem().toString().equals("Holiday")){
					
						control.updateState("Holiday", "10.30-12.30");
						j.remove(timew);
						j.add(timeh);
				}
			
			}
		});
		
		JButton submit = new JButton("SUBMIT");
		submit.setBounds(510, 590, 80, 30);
		
		submit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				if(control.getDateState().equals("Weekday")){
					if(!control.getTimeState().equals("10.30-12.30") && 
							timew.getSelectedItem().toString().equals("10.30-12.30")){
						control.updateState("Weekday", "10.30-12.30");
					}
					else if(!control.getTimeState().equals("12.30-14.30") &&
							timew.getSelectedItem().toString().equals("12.30-14.30")) {
						control.updateState("Weekday", "12.30-14.30");
					}
				}
				else {
					if(!control.getTimeState().equals("10.30-12.30") &&
							timeh.getSelectedItem().toString().equals("10.30-12.30")){
						control.updateState("Holiday", "10.30-12.30");
					}
					else if(!control.getTimeState().equals("12.30-14.30") &&
							timeh.getSelectedItem().toString().equals("12.30-14.30")) {
						control.updateState("Holiday", "12.30-14.30");
					}
					else if(!control.getTimeState().equals("17.00-19.00") &&
							timeh.getSelectedItem().toString().equals("17.00-19.00")) {
						control.updateState("Holiday", "17.00-19.00");
					}
					
				}
			}
		});
		
		j.add(l0);
		j.add(l1);
		j.add(l2);
		j.add(l3);
		j.add(l4);
		j.add(l5);
		j.add(l6);
		j.add(l7);
		j.add(l8);
		j.add(l9);
		j.add(l10);
		j.add(l11);
		j.add(l12);
		j.add(l13);
		j.add(l14);
		j.add(day);
		j.add(ok1);
		j.add(submit);
		j.add(buy);
		j.add(clear);
		j.add(total);
		j.add(p1);

		if (key == 1){
			j.add(timew);
		}

		j.setVisible(true);
	}
	public ArrayList<JCheckBox> setSeats(){
		return seats;
	}
	public void showTotal() {
		total.setText("Total Price :  " + control.getTotal() + "  baht.");
	}
}
